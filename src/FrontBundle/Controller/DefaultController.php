<?php

namespace App\FrontBundle\Controller;

use App\FrontBundle\Form\ContactoType;
use App\FrontBundle\Form\ContactoSeoType;
use App\BackendBundle\Entity\ea__banners;
use App\BackendBundle\Entity\ea__categorias;
use App\BackendBundle\Entity\ea__producto;
use App\BackendBundle\Entity\ea__blog;
use App\Application\Sonata\MediaBundle\Entity\Gallery;
use App\Application\Sonata\MediaBundle\Entity\GalleryHasMedia;
use App\Application\Sonata\MediaBundle\Entity\Media;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Common\Collections\Expr\Expression;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    /**
     * @Cache(smaxage="3600", maxage="15")
     */
    public function indexAction(Request $request): Response
    {

        $banners = $this->getDoctrine()->getRepository(ea__banners::class)->findBy(array(),array('posicion' => 'ASC'));
        $noticias = $this->getDoctrine()->getRepository(ea__blog::class)->findBy([],['fecha'=>'DESC']);
//         $crawler = $request->request(
//     'GET',
//     '/homepage',
//     array(),
//     array(),
//     array('HTTP_HOST' => 'currus.' . $request->getContainer()->getParameter('domain'))
// );

        return $this->render('@Front/Default/index.html.twig',array(
            'categorias'=>  $this->getCategories(),
            'banners'   =>  $banners,
            'destacados'=>  $this->getProductsbyCategory(9),
            'hotsale'   =>  $this->getProductsbyCategory(1),
            'accesorios'=>  $this->getProductsbyCategory(7),
            'noticias'  =>  $noticias
        ));
    }

    public function categoriaAction(Request $request, $categoria = null){
        $categoriaStr = str_replace("-", " ", $categoria);
        
        if($categoria == null){
       
            $productos = [];
            $msj = 'La categoria que seleccionaste no existe';
        
        }else{
        
            $categoriaData = $this->getDoctrine()->getRepository(ea__categorias::class)->findOneBy(array('nombre'=>$categoriaStr));
            $productos = $this->getProductsbyCategory($categoriaStr);
        }

        return $this->render('@Front/Default/categoria.html.twig',array(
            'categorias'=>  $this->getCategories(),
            'catData'   =>  $categoriaData,
            'productos' =>  $productos
        ));

    }

     public function productoAction(Request $request, $producto=null, $id){

        $producto = $this->getDoctrine()->getRepository(ea__producto::class)->findOneById($id);
        if ($producto->getImagenes() != null) {
            $gallery = $this->getDoctrine()->getRepository(GalleryHasMedia::class)->findByGallery($producto->getImagenes()->getId());
        }else{
            $gallery = null;
        }
        $currentCategory = $producto->getCategoria()[0]->getId();

        return $this->render('@Front/Default/single.html.twig',array(
            'categorias' => $this->getCategories(),
            'producto'   => $producto,
            'gallery'    => $gallery,
            'related'    => $this->getProductsbyCategory($currentCategory),
        ));

    }

    public function posventaAction(Request $request){

        return $this->render('@Front/Default/posventa.html.twig',array(
            'categorias' =>  $this->getCategories(),
            'accesorios' =>  $this->getProductsbyCategory(7)
        ));
    }

    public function contactoAction(Request $request, \Swift_Mailer $mailer){
        $form = $this->createForm(ContactoType::class);
        
        if ($request->isMethod('POST')) {
            // Refill the fields in case the form is not valid.
            $form->handleRequest($request);

            if($form->isValid()){
                $data = $form->getData();
                // Send mail
                $message = (new \Swift_Message('Ecoalsina Contacto'))
                ->setFrom('noreply@ecoalsina.com')
                ->setTo('alsinamotos@gmail.com')
                ->setBcc('julietabel@gmail.com')
                ->setBody('"'.$data["mensaje"].'" '.' Email de contacto : '.$data["email"] );

                $mail = $mailer->send($message);

                if($mail){
                    return $this->redirectToRoute('app_contacto_validate', [
                        'val' => 'ok'
                    ]);
                }else{
                    return $this->redirectToRoute('app_contacto_validate', [
                        'val' => 'ko'
                    ]);
                }
            }
        }
        return $this->render('@Front/Default/contacto.html.twig',array(
            'categorias' =>   $this->getCategories(),
            'form'       =>   $form->createView()
        ));

    }


    public function blogAction(Request $request){
        $noticias = $this->getDoctrine()->getRepository(ea__blog::class)->findBy([],['fecha'=>'DESC']);
        
        return $this->render('@Front/Default/blog.html.twig',array(
            'categorias' =>   $this->getCategories(),
            'noticias'   =>   $noticias
        ));                   

    }
    
    public function noticiaAction($titulo,$id){
        $noticia = $this->getDoctrine()->getRepository(ea__blog::class)->findById($id);

        return $this->render('@Front/Default/noticia.html.twig',array(
            'categorias' =>   $this->getCategories(),
            'noticia'   =>   $noticia
        ));  
    
    }


    /* conect whit repositories */
    public function getCategories(){
        return $this->getDoctrine()->getRepository(ea__categorias::class)->getAllByOrderAsc();
    }

    public function getProductsbyCategory($category){
        if(is_string($category)){
            return $this->getDoctrine()->getRepository(ea__producto::class)->getProductsbyCategoryName($category);
        }else{
            return $this->getDoctrine()->getRepository(ea__producto::class)->getProductsbyCategoryId($category);
        }
    }

    public function landingAction(Request $request, \Swift_Mailer $mailer){
        $form = $this->createForm(ContactoSeoType::class);
        
        if ($request->isMethod('POST')) {
            // Refill the fields in case the form is not valid.
            $form->handleRequest($request);

            if($form->isValid()){
                $data = $form->getData();
                // Send mail
                $message = (new \Swift_Message('Ecoalsina Landing'))
                ->setFrom('noreply@ecoalsina.com')
                ->setTo('alsinamotos@gmail.com')
                ->setBcc('julietabel@gmail.com')
                ->setBody('Producto : '.$data['producto'].' Mensaje: "'.$data["mensaje"].'"'.
                    ' Email de contacto : '.$data["email"].
                    ' Telefono de contacto : '.$data["telefono"] );

                $mail = $mailer->send($message);

                if($mail){
                    return $this->redirectToRoute('app_homepage', ['send'=> '1']);
                }else{
                    return $this->redirectToRoute('app_contacto_validate', [
                        'val' => 'ko'
                    ]);
                }
            }
        }
        // $noticias = $this->getDoctrine()->getRepository(ea__blog::class)->findBy([],['fecha'=>'DESC']);
        
        return $this->render('@Front/Default/landing.html.twig',array(
            // 'categorias' =>   $this->getCategories(),
            // 'noticias'   =>   $noticias
             'form'       =>   $form->createView()
        ));             
    }      
       public function rssAction(Request $request){

    
            $productos = $this->getDoctrine()->getRepository(ea__producto::class)->findAll();
   

            $response = new Response();
            $response->headers->set('Content-Type', 'text/xml');
            return $this->render(
               '@Front/Default/rss_fb.xml.twig',
               array(
                    'productos' =>  $productos
               ),
               $response
            );
     

    } 
    public function rssInstagramAction(Request $request){

    
            $productos = $this->getDoctrine()->getRepository(ea__producto::class)->findAll();
   

            $response = new Response();
            $response->headers->set('Content-Type', 'text/xml');
            return $this->render(
               '@Front/Default/rss_ig.xml.twig',
               array(
                    'productos' =>  $productos
               ),
               $response
            );
     

    } 
}
