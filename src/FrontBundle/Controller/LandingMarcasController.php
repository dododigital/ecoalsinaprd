<?php

namespace App\FrontBundle\Controller;

use App\FrontBundle\Form\ContactoType;
use App\FrontBundle\Form\ContactoSeoType;
use App\BackendBundle\Entity\ea__banners;
use App\BackendBundle\Entity\ea__categorias;
use App\BackendBundle\Entity\ea__producto;
use App\BackendBundle\Entity\ea__blog;
use App\Application\Sonata\MediaBundle\Entity\Gallery;
use App\Application\Sonata\MediaBundle\Entity\GalleryHasMedia;
use App\Application\Sonata\MediaBundle\Entity\Media;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Common\Collections\Expr\Expression;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Symfony\Component\HttpFoundation\Response;

class LandingMarcasController extends Controller
{
    /**
     * @Cache(smaxage="3600", maxage="15")
     */
    public function landingAction(Request $request): Response
    {
        $marca = $request->get('subdomain');
        if ($marca == 'currus') {
            return $this->redirectToRoute('app_homepage', [], 301);
        }
        return $this->render('@Front/LandingMarcas/'.$marca.'/index.html.twig',array(
            'marca'=>  $marca,
            'categorias'=>  $this->getCategories(),
            'destacados'=>  $this->getProductsbyCategory(9),
            'hotsale'   =>  $this->getProductsbyCategory(1),
            'accesorios'=>  $this->getProductsbyCategory(7),
        ));
    }

   /**
     * @Cache(smaxage="3600", maxage="15")
     */
    public function kabooAction(Request $request): Response
    {
        $marca = $request->get('subdomain');
        
        return $this->render('@Front/Currus/index.html.twig',array(
            'marca'=>  $marca,
            'categorias'=>  $this->getCategories(),
            'destacados'=>  $this->getProductsbyCategory(9),
            'hotsale'   =>  $this->getProductsbyCategory(1),
            'accesorios'=>  $this->getProductsbyCategory(7),
        ));
    }

  
    /* conect whit repositories */
    public function getCategories(){
        return $this->getDoctrine()->getRepository(ea__categorias::class)->getAllByOrderAsc();
    }

    public function getProductsbyCategory($category){
        if(is_string($category)){
            return $this->getDoctrine()->getRepository(ea__producto::class)->getProductsbyCategoryName($category);
        }else{
            return $this->getDoctrine()->getRepository(ea__producto::class)->getProductsbyCategoryId($category);
        }
    }
}
