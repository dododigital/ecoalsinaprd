<?php

namespace App\FrontBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class ContactoSeoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre', null,array(
                'attr' => array(
                    'class' => 'form-control',
                    // 'placeholder' => 'Nombre'
                )))
            ->add('telefono', null,array(
                'attr' => array(
                    'class' => 'form-control',
                    // 'placeholder' => 'Telefono'
                )))
            ->add('email',null,array(
                'attr' => array(
                    'class' => 'form-control',
                    // 'placeholder' => 'Email'
                )))
            ->add('producto', ChoiceType::class ,array(
                'choices' => array(
                    'Monopatines' => 'monopatines',
                    'Citycoco' => 'citycoco',
                    'Scooters' => 'scooters',
                    'Triciclos' => 'triciclos',
                    'Calle' => 'calle',
                    'Repuestos y accesorios' => 'accesorios',
                    'Otro' => 'otro'
                ),
                'attr' => array(
                    'class' => 'form-control',
                    // 'placeholder' => 'Email'
                )))
            ->add('mensaje', TextAreaType::class ,array(
                'attr' => array(
                    'rows' => 4,
                    'class' => 'form-control md-textarea',
                    // 'placeholder' => 'Mensaje',
                    'style' => 'resize:none;text-align:left;'
                )))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // uncomment if you want to bind to a class
            // 'data_class' => ContactoSeoType::class,
        ]);
    }
}