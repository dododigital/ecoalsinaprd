<?php

namespace App\FrontBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class ContactoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre', null,array(
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => 'Nombre'
                )))
            ->add('email',null,array(
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => 'Email'
                )))
            ->add('asunto',null,array(
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => 'Asunto'
                )))
            ->add('mensaje', TextAreaType::class ,array(
                'attr' => array(
                    'rows' => 4,
                    'class' => 'form-control md-textarea',
                    'placeholder' => 'Mensaje',
                    'style' => 'resize:none;'
                )))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // uncomment if you want to bind to a class
            //'data_class' => Contact::class,
        ]);
    }
}