<?php

namespace App\Application\Sonata\MediaBundle\Provider;

use SilasJoisten\Sonata\MultiUploadBundle\Traits\MultiUploadTrait;
use Sonata\MediaBundle\Provider\ImageProvider;
use Sonata\MediaBundle\Provider\FileProvider;
use Sonata\MediaBundle\Provider\Metadata;
use Sonata\MediaBundle\Resizer\ResizerInterface;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Gaufrette\Filesystem;
use Sonata\MediaBundle\CDN\CDNInterface;
use Sonata\MediaBundle\Generator\GeneratorInterface;
use Buzz\Browser;
use Sonata\MediaBundle\Thumbnail\ThumbnailInterface;
use Sonata\MediaBundle\Metadata\MetadataBuilderInterface;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;
use Sonata\MediaBundle\Model\MediaInterface;

class MultiProvider extends FileProvider
{
    use MultiUploadTrait;
    //     /**
    //  * @var ResizerInterface
    //  */
    // protected $resizer;

    public function getProviderMetadata()
    {
        return new Metadata($this->getName(),
            $this->getName().'.description',
            null,
            'SonataMediaBundle',
            ['class' => 'fa fa-files-o']
        );
    }
}