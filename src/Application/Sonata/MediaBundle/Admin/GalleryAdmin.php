<?php

declare(strict_types=1);

/*
 * This file is part of the Sonata Project package.
 *
 * (c) Thomas Rabaix <thomas.rabaix@sonata-project.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Application\Sonata\MediaBundle\Admin;

use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\MediaBundle\Admin\GalleryAdmin as Admin;
use Sonata\AdminBundle\Route\RouteCollection;  
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\Form\Type\CollectionType;
use Sonata\MediaBundle\Provider\Pool;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

/**
 * @final since sonata-project/media-bundle 3.21.0
 */
class GalleryAdmin extends Admin
{
    /**
     * {@inheritdoc}
     */
     protected $datagridValues = [
        '_page' => 1,        '_sort_order' => 'DESC',
        '_sort_by' => 'updatedAt',
    ];

        protected function configureFormFields(FormMapper $formMapper)
    {
        // define group zoning
        $formMapper
            ->with('Gallery', ['class' => 'col-md-9'])->end()
            ->with('Options', ['class' => 'col-md-3'])->end()
        ;

        $context = $this->getPersistentParameter('context');

        if (!$context) {
            $context = $this->pool->getDefaultContext();
        }

        $formats = [];
        foreach ((array) $this->pool->getFormatNamesByContext($context) as $name => $options) {
            $formats[$name] = $name;
        }

        $contexts = [];
        foreach ((array) $this->pool->getContexts() as $contextItem => $format) {
            $contexts[$contextItem] = $contextItem;
        }

        $formMapper
            ->with('Options')
                ->add('context', ChoiceType::class, ['choices' => $contexts])
                ->add('enabled', null, ['required' => false])
                ->add('name')
                ->ifTrue($formats)
                    ->add('defaultFormat', ChoiceType::class, ['choices' => $formats])
                ->ifEnd()
            ->end()
            ->with('Gallery')
                ->add('galleryHasMedias', CollectionType::class, ['by_reference' => false], [
                    'edit' => 'inline',
                    'inline' => 'table',
                    'sortable' => 'position',
                    // 'link_parameters' => ['provider'=>'sonata.media.provider.image','context' => $context],
                    'admin_code' => 'sonata.media.admin.gallery_has_media',
                ])
            ->end()
        ;
    }
    protected function configureRoutes(RouteCollection $collection): void
    {
        $collection->add('create_gallery', 'create/gallery/uploaded/medias');
    }
}
