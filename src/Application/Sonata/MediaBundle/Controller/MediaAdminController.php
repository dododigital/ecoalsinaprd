<?php
namespace App\Application\Sonata\MediaBundle\Controller;

use Sonata\MediaBundle\Admin\GalleryAdmin;
use SilasJoisten\Sonata\MultiUploadBundle\Controller\MultiUploadController;
use Sonata\MediaBundle\Entity\MediaManager;
use Sonata\MediaBundle\Entity\GalleryManager;


final class MediaAdminController extends MultiUploadController
{
    public function createGalleryAction(Request $request, MediaManager $mediaManager, GalleryManager $galleryManager, GalleryAdmin $galleryAdmin): RedirectResponse
    {
        $idx = $request->query->get('idx');
        $idx = json_decode($idx);
    
        $gallery = $galleryManager->create();
        $gallery->setName('Auto Created Gallery');
        $gallery->setEnabled(false);
        $gallery->setContext('default');
    
        foreach ($idx as $id) {
            $media = $mediaManager->find($id);
            
            $galleryHasMedia = new GalleryHasMedia();
            $galleryHasMedia->setGallery($gallery);
            $galleryHasMedia->setMedia($media);
            $gallery->addGalleryHasMedia($galleryHasMedia);
        }
    
        $galleryManager->save($gallery);
    
        return $this->redirect($galleryAdmin->generateObjectUrl('edit', $gallery));
    }
}