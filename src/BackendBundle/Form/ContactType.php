<?php

namespace App\BackendBundle\Form;

use BackendBundle\Entity\Contact;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContactType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre', null,array(
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => 'Nombre'
                )))
            ->add('email',null,array(
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => 'Email'
                )))
            ->add('asunto',null,array(
                'attr' => array(
                    'class' => 'form-control',
                    'placeholder' => 'Asunto'
                )))
            ->add('mensaje', 'textarea' ,array(
                'attr' => array(
                    'class' => 'form-control md-textarea',
                    'placeholder' => 'Mensaje'
                )))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // uncomment if you want to bind to a class
            //'data_class' => Contact::class,
        ]);
    }
}