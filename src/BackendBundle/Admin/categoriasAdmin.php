<?php

namespace App\BackendBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Form\Type\ModelListType;

class categoriasAdmin extends AbstractAdmin
{
     protected $listValues = [
        '_sort_order' => 'ASC',
        '_sort_by' => 'orden',
    ];
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('nombre')
            ->add('activarPrecio')
            ->add('orden')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('nombre')
            ->add('activarPrecio')
            ->add('orden')
            ->add('imagen',null,array('label'=>'Imagen de portada', 'template' => 'SonataMediaBundle:MediaAdmin:list_image.html.twig'))
            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],
            ])
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('nombre')
            ->add('activarPrecio', null, array('value' => true, 'required' => false))
            ->add('orden', null, array('required'=>false, 'help'=> '*Dejar el orden vacio si NO forma parte del menú de vehiculos (Ej.: destacados, hot sale)'))
            ->add('imagen' ,ModelListType::class, [
                        'label' => 'Imagen de portada',
                    ], [
                        'link_parameters' => array('provider' => 'sonata.media.provider.image', 'context' => 'imagen')
                    ])
        ;
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('nombre')
            ->add('activarPrecio')
        ;
    }
}
