<?php

namespace App\BackendBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ChoiceFieldMaskType;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\CoreBundle\Form\Type\DatePickerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Sonata\AdminBundle\Form\Type\ModelType;
use Sonata\AdminBundle\Form\Type\ModelListType;

class bannersAdmin extends AbstractAdmin
{
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('nombre')
            ->add('titulo')
            ->add('bajada')
            ->add('imagen')
            ->add('posicion')
            // ->add('fecha','doctrine_orm_datetime', ['field_type'=> DatePickerType::class])
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('nombre')
            ->add('titulo')
            ->add('bajada')
            ->add('imagen', null, array('label'=>'Imagen/Gif', 'template' => 'SonataMediaBundle:MediaAdmin:list_image.html.twig'))
            ->add('posicion')
            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],
            ])
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('nombre')
            ->add('titulo')
            ->add('bajada')
            ->add('posicion')          
            ->add('imagen', ModelListType::class, [
                        'label' => 'Imagen',
                    ], [
                        'link_parameters' => array('provider' => 'sonata.media.provider.image', 'context' => 'imagen')
                    ])
        ;
    }
  public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'btn_add' => null,
        ]);
    }
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('nombre')
            ->add('titulo')
            ->add('bajada')
            ->add('posicion') 
            ->add('imagen') 
        ;
    }
}
