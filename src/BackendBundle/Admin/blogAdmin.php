<?php

namespace App\BackendBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ChoiceFieldMaskType;
use Sonata\AdminBundle\Show\ShowMapper;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Sonata\CoreBundle\Form\Type\DatePickerType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Sonata\CoreBundle\Form\Type\ColorSelectorType;
use Sonata\CoreBundle\Form\Type\ColorType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Sonata\CoreBundle\Color\Colors;
use Sonata\Form\Type\CollectionType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Sonata\AdminBundle\Form\Type\ModelType;
use Sonata\AdminBundle\Form\Type\ModelAutocompleteType;
use Sonata\AdminBundle\Form\Type\ModelListType;

class blogAdmin extends AbstractAdmin
{
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('titulo')
            ->add('bajada')
            ->add('cuerpo')
            ->add('galeria', null, array('label'=>'Galeria'))
            ->add('fecha','doctrine_orm_datetime', ['field_type'=> DatePickerType::class])
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {

        $listMapper
            ->add('imgprincipal', null,array('label'=>'Imagen', 'template' => 'SonataMediaBundle:MediaAdmin:list_image.html.twig'))
            ->add('titulo')
            ->add('bajada')
            // ->add('cuerpo')
            ->add('tags')
            ->add('producto')
            ->add('galeria', null, array('label'=>'Galeria'))
            ->add('orden')
            ->add('fecha',null,array('label'=>'Fecha creación','format' => 'd/m/y'))
            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],
            ])
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('General', ['class' => 'col-md-8'])
                ->add('titulo')
                ->add('bajada', null, array('required'=> true))              
                ->add('cuerpo',TextareaType::class,
                    array('attr' => array(
                    'class' => 'tinymce',
                    'data-theme' => 'advanced'// Skip it if you want to use default theme
                )))
                ->add('galeria', ModelListType::class, ['label' => 'Galeria'], 
                        [
                        'link_parameters' => array(
                            // 'provider' => 'App\Application\Sonata\MediaBundle\Provider\MultiProvider',
                            'context' => 'imagen')
                        ])
                ->add('imgprincipal', ModelListType::class, ['label' => 'Imagen Principal'], 
                        [
                            'link_parameters' => array(
                                'providers' =>[ 'sonata.media.provider.image','sonata.media.provider.youtube'],
                                'context' => 'imagen')
                        ])
            ->end()

            ->with('Opciones',['class' => 'col-md-4'])
                ->add('tags', ModelAutocompleteType::class, [
                    'property' => 'nombre',
                    'btn_add' => "Nuevo tag",
                    'multiple' => true
                ])
                ->add('producto')
                ->add('orden')
                ->add('fecha', DatePickerType::class)
            ->end()
        ;
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('imgprincipal', null,array('label'=>'Imagen', 'template' => 'SonataMediaBundle:MediaAdmin:list_image.html.twig'))
            ->add('titulo')
            ->add('bajada')
            ->add('cuerpo')
            ->add('tags')
            ->add('producto')
            ->add('galeria', null, array('label'=>'Galeria'))
            ->add('orden')
        ;
    }
}
