<?php

namespace App\BackendBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Form\Type\ModelListType;

class beneficioAdmin extends AbstractAdmin
{
     protected $listValues = [
        '_sort_order' => 'ASC',
        '_sort_by' => 'orden',
    ];
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('nombre')
            // ->add('activarPrecio')
            // ->add('orden')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('nombre')
            ->add('icono',null,array('label'=>'Icono', 'template' => 'SonataMediaBundle:MediaAdmin:list_image.html.twig'))
            // ->add('orden')
            // ->add('imagen')
            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],
            ])
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('nombre')
            // ->add('activarPrecio', null, array('value' => true, 'required' => false))
            // ->add('orden', null, array('required'=>false))
            ->add('icono' ,ModelListType::class, [
                        'label' => 'Imagen',
                    ], [
                        'link_parameters' => array('provider' => 'sonata.media.provider.image', 'context' => 'imagen')
                    ])
        ;
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('nombre')
            // ->add('activarPrecio')
        ;
    }
}
