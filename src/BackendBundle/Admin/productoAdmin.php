<?php

namespace App\BackendBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ChoiceFieldMaskType;
use Sonata\AdminBundle\Show\ShowMapper;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Sonata\CoreBundle\Form\Type\DatePickerType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Sonata\CoreBundle\Form\Type\ColorSelectorType;
use Sonata\CoreBundle\Form\Type\ColorType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Sonata\CoreBundle\Color\Colors;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Sonata\AdminBundle\Form\Type\ModelType;
use Sonata\AdminBundle\Form\Type\ModelListType;

class productoAdmin extends AbstractAdmin
{
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('nombre')
            ->add('descripcion')
            ->add('precio')
            ->add('imagenes', null, array('label'=>'Galeria'))
            ->add('categoria')
            ->add('marca')
            ->add('beneficios')
            ->add('oferta')
            ->add('fecha','doctrine_orm_datetime', ['field_type'=> DatePickerType::class])
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {

        $listMapper
            ->add('imgprincipal', null,array('label'=>'Imagen', 'template' => 'SonataMediaBundle:MediaAdmin:list_image.html.twig'))
            ->add('nombre')
            ->add('descripcion')
            ->add('precio')
            ->add('beneficios')
            ->add('categoria')
            ->add('marca')
            ->add('imagenes', null, array('label'=>'Galeria'))
            ->add('oferta')
            ->add('fecha',null,array('label'=>'Fecha creación','format' => 'd/m/y'))
            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],
            ])
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $colores = array_unique(Colors::getAll());

        $formMapper
            ->tab('General')
                ->with('General', ['class' => 'col-md-8'])
                    ->add('nombre')
                    ->add('descripcion',TextareaType::class)
                    //         array('attr' => array(
                    //     'class' => 'tinymce',
                    //     'data-theme' => 'advanced'// Skip it if you want to use default theme
                    // )
                    ->add('categoria', null, array('multiple'=> true))              
                    ->add('imagenes', ModelListType::class, ['label' => 'Galeria'], 
                            [
                            'link_parameters' => array(
                                // 'provider' => 'App\Application\Sonata\MediaBundle\Provider\MultiProvider',
                                'context' => 'imagen')
                            ])
                    ->add('imgprincipal', ModelListType::class, ['label' => 'Imagen Principal'], 
                            [
                                'link_parameters' => array(
                                    'provider' => 'sonata.media.provider.image',
                                    'context' => 'imagen')
                            ])
                    ->add('beneficios',ModelType::class, array('multiple'=> true))
                ->end()

                ->with('Opciones',['class' => 'col-md-4'])
                    ->add('oferta')
                    ->add('precio')
                    ->add('marca')
                    ->add('pdf', ModelListType::class, ['label' => 'Ficha PDF'], 
                            [
                                'link_parameters' => array(
                                    'provider' => 'sonata.media.provider.file',
                                    'context' => 'imagen')
                            ],
                            [
                                'edit' => 'inline'
                            ]
                        )
                    ->add('colores',   ColorSelectorType::class,  ['multiple'=>true, 'preferred_choices' => [] ])
                    ->add('fecha', DatePickerType::class)
                ->end()
            ->end()

            
            ->tab('Ficha técnica')
                ->with('  ', ['class' => 'col-md-6'])
                    ->add('motor', null, array('label'=> 'Marca Motor'))
                    ->add('valocidadMax', null, array('label'=> 'Velocidad Maxima'))
                    ->add('autonomia', null, array('label'=> 'Autonomía'))
                    ->add('bateria', null, array('label' => 'Batería'))
                    ->add('tiempoCarga', null, array('label' => 'Tiempo de carga'))
                ->end()
                ->with(' ', ['class' => 'col-md-6'])
                    ->add('frenos', null, array('label' => 'Frenos'))
                    ->add('equipamiento', null, array('label' => 'Equipamiento / Opcionales'))
                    ->add('peso', null, array('label' => 'Peso vehiculo'))
                    ->add('pesoMax', null, array('label' => 'Peso Maximo'))
                    ->add('cubiertas', null, array('label' => 'Cubiertas'))   
                ->end()
            ->end()
        ;
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('nombre')
            ->add('descripcion')
            ->add('precio')
            ->add('imagenes')
            ->add('categoria')
            ->add('beneficios')
            ->add('oferta')
            ->add('pdf')
            ->add('colores')
            ->add('fecha')
        ;
    }
}
