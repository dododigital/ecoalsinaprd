<?php

namespace App\BackendBundle\Entity;

use App\Application\Sonata\MediaBundle\Entity\Media;

class ea__categorias
{
    private $id;

    private $nombre;

    private $activarPrecio;

    private $orden;
    
    private $imagen;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getActivarPrecio(): ?bool
    {
        return $this->activarPrecio;
    }

    public function setActivarPrecio(bool $activarPrecio): self
    {
        $this->activarPrecio = $activarPrecio;

        return $this;
    }
        public function __toString()
    {
        return (string) $this->nombre;
    }

        public function getOrden(): ?int
        {
            return $this->orden;
        }

        public function setOrden(int $orden = null): self
        {
            $this->orden = $orden;

            return $this;
        }

        public function getImagen(): ?Media
        {
            return $this->imagen;
        }

        public function setImagen(?Media $imagen): self
        {
            $this->imagen = $imagen;

            return $this;
        }
}
