<?php

namespace App\BackendBundle\Entity;

use App\Application\Sonata\MediaBundle\Entity\Gallery;
use App\Application\Sonata\MediaBundle\Entity\Media;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

class ea__blog
{
    private $id;

    private $titulo;

    private $bajada;

    private $descripcion;

    private $cuerpo;

    private $fecha;

    private $orden;

    private $tags;
    
    private $producto;

    private $galeria;
    
    private $imgprincipal;

    public function __construct()
    {
        $this->tags = new ArrayCollection();
        $this->producto = new ArrayCollection();
        $this->fecha = new \DateTime();
    }

    public function __toString()
    {
        return (string) $this->titulo;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitulo(): ?string
    {
        return $this->titulo;
    }

    public function setTitulo(string $titulo): self
    {
        $this->titulo = $titulo;

        return $this;
    }

    public function getBajada(): ?string
    {
        return $this->bajada;
    }

    public function setBajada(?string $bajada): self
    {
        $this->bajada = $bajada;

        return $this;
    }

    public function getCuerpo(): ?string
    {
        return $this->cuerpo;
    }

    public function setCuerpo(?string $cuerpo): self
    {
        $this->cuerpo = $cuerpo;

        return $this;
    }

    public function getFecha(): ?\DateTimeInterface
    {
        return $this->fecha;
    }

    public function setFecha(\DateTimeInterface $fecha): self
    {
        $this->fecha = $fecha;

        return $this;
    }

    public function getOrden(): ?int
    {
        return $this->orden;
    }

    public function setOrden(?int $orden): self
    {
        $this->orden = $orden;

        return $this;
    }

    public function getGaleria(): ?Gallery
    {
        return $this->galeria;
    }

    public function setGaleria(?Gallery $galeria): self
    {
        $this->galeria = $galeria;

        return $this;
    }

    public function getImgprincipal(): ?Media
    {
        return $this->imgprincipal;
    }

    public function setImgprincipal(?Media $imgprincipal): self
    {
        $this->imgprincipal = $imgprincipal;

        return $this;
    }

    /**
     * @return Collection|ea__tags[]
     */
    public function getTags(): Collection
    {
        return $this->tags;
    }

    public function addTag(ea__tags $tag): self
    {
        if (!$this->tags->contains($tag)) {
            $this->tags[] = $tag;
        }

        return $this;
    }

    public function removeTag(ea__tags $tag): self
    {
        if ($this->tags->contains($tag)) {
            $this->tags->removeElement($tag);
        }

        return $this;
    }

    /**
     * @return Collection|ea__producto[]
     */
    public function getProducto(): Collection
    {
        return $this->producto;
    }

    public function addProducto(ea__producto $producto): self
    {
        if (!$this->producto->contains($producto)) {
            $this->producto[] = $producto;
        }

        return $this;
    }

    public function removeProducto(ea__producto $producto): self
    {
        if ($this->producto->contains($producto)) {
            $this->producto->removeElement($producto);
        }

        return $this;
    }
}
