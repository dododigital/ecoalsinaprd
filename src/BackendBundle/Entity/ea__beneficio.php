<?php

namespace App\BackendBundle\Entity;

use App\Application\Sonata\MediaBundle\Entity\Media;

class ea__beneficio
{
    private $id;

    private $nombre;

    private $icono;

    private $descripcion;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getIcono(): ?Media
    {
        return $this->icono;
    }

    public function setIcono(?Media $icono): self
    {
        $this->icono = $icono;

        return $this;
    }

    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    public function setDescripcion(?string $descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }
    public function __toString()
    {
        return (string) $this->nombre;
    }
}
