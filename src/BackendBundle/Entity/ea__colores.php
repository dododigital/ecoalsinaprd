<?php

namespace App\BackendBundle\Entity;

use App\Application\Sonata\MediaBundle\Entity\Gallery;
use App\Application\Sonata\MediaBundle\Entity\Media;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

class ea__colores
{
    private $id;

    private $hexa;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getHexa(): ?ea__producto
    {
        return $this->hexa;
    }

    public function setHexa(?ea__producto $hexa): self
    {
        $this->hexa = $hexa;

        return $this;
    }


}
