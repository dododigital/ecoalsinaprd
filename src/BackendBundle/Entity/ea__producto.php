<?php

namespace App\BackendBundle\Entity;

use App\Application\Sonata\MediaBundle\Entity\Gallery;
use App\Application\Sonata\MediaBundle\Entity\Media;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

class ea__producto
{
    private $id;

    private $nombre;

    private $descripcion;

    private $precio;

    private $beneficios;

    private $oferta;

    private $pdf;

    private $colores;
    
    private $marca;

    private $fecha;

    private $imagenes;
    
    private $imgprincipal;

    private $categoria;

    private $motor;    
    
    private $valocidadMax;
    
    private $autonomia;
    
    private $bateria;
    
    private $tiempoCarga;
    
    private $frenos;
    
    private $equipamiento;
    
    private $peso;
    
    private $pesoMax;
    
    private $cubiertas;

    public function __construct()
    {
        $this->categoria = new ArrayCollection();
        $this->beneficios = new ArrayCollection();
        $this->fecha = new \DateTime();
    }
    
    public function prePersist()
    {
        $this->fecha = new \DateTime();
    }

    public function preUpdate()
    {
        $this->fecha = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    public function setDescripcion(?string $descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getPrecio(): ?int
    {
        return $this->precio;
    }

    public function setPrecio(?int $precio): self
    {
        $this->precio = $precio;

        return $this;
    }


    public function getFecha(): ?\DateTimeInterface
    {
        return $this->fecha;
    }

    public function setFecha(\DateTimeInterface $fecha): self
    {
        $this->fecha = $fecha;

        return $this;
    }

    public function getImagenes(): ?Gallery
    {
        return $this->imagenes;
    }

        public function setImagenes(?Gallery $imagenes): self
        {
            $this->imagenes = $imagenes;

            return $this;
    }

    /**
     * @return Collection|ea__categorias[]
     */
    public function getCategoria(): Collection
    {
        return $this->categoria;
    }

    public function addCategorium(ea__categorias $categorium): self
    {
        if (!$this->categoria->contains($categorium)) {
            $this->categoria[] = $categorium;
        }

        return $this;
    }

    public function removeCategorium(ea__categorias $categorium): self
    {
        if ($this->categoria->contains($categorium)) {
            $this->categoria->removeElement($categorium);
        }

        return $this;
    }
    public function __toString()
    {
        return (string) $this->nombre;
    }

    public function getImgprincipal(): ?Media
    {
        return $this->imgprincipal;
    }

    public function setImgprincipal(?Media $imgprincipal): self
    {
        $this->imgprincipal = $imgprincipal;

        return $this;
    }

    /**
     * @return Collection|ea__beneficio[]
     */
    public function getBeneficios(): Collection
    {
        return $this->beneficios;
    }

    public function addBeneficio(ea__beneficio $beneficio): self
    {
        if (!$this->beneficios->contains($beneficio)) {
            $this->beneficios[] = $beneficio;
        }

        return $this;
    }

    public function removeBeneficio(ea__beneficio $beneficio): self
    {
        if ($this->beneficios->contains($beneficio)) {
            $this->beneficios->removeElement($beneficio);
        }

        return $this;
    }

    public function getColores(): ?array
    {
        return $this->colores;
    }

    public function setColores(?array $colores): self
    {
        $this->colores = $colores;

        return $this;
    }

    public function getMarca(): ?string
    {
        return $this->marca;
    }

    public function setMarca(?string $marca): self
    {
        $this->marca = $marca;

        return $this;
    }

    public function getPdf(): ?Media
    {
        return $this->pdf;
    }

    public function setPdf(?Media $pdf): self
    {
        $this->pdf = $pdf;

        return $this;
    }

    public function getOferta()
    {
        return $this->oferta;
    }

    public function setOferta($oferta): self
    {
        $this->oferta = $oferta;

        return $this;
    }

    public function getMotorMarca(): ?string
    {
        return $this->motorMarca;
    }

    public function setMotorMarca(?string $motorMarca): self
    {
        $this->motorMarca = $motorMarca;

        return $this;
    }

    public function getMotorPotencia(): ?string
    {
        return $this->motorPotencia;
    }

    public function setMotorPotencia(?string $motorPotencia): self
    {
        $this->motorPotencia = $motorPotencia;

        return $this;
    }

    public function getValocidadMax(): ?string
    {
        return $this->valocidadMax;
    }

    public function setValocidadMax(?string $valocidadMax): self
    {
        $this->valocidadMax = $valocidadMax;

        return $this;
    }

    public function getAutonomia(): ?string
    {
        return $this->autonomia;
    }

    public function setAutonomia(?string $autonomia): self
    {
        $this->autonomia = $autonomia;

        return $this;
    }

    public function getBateria(): ?string
    {
        return $this->bateria;
    }

    public function setBateria(?string $bateria): self
    {
        $this->bateria = $bateria;

        return $this;
    }

    public function getTiempoCarga(): ?string
    {
        return $this->tiempoCarga;
    }

    public function setTiempoCarga(?string $tiempoCarga): self
    {
        $this->tiempoCarga = $tiempoCarga;

        return $this;
    }

    public function getFrenos(): ?string
    {
        return $this->frenos;
    }

    public function setFrenos(?string $frenos): self
    {
        $this->frenos = $frenos;

        return $this;
    }

    public function getEquipamiento(): ?string
    {
        return $this->equipamiento;
    }

    public function setEquipamiento(?string $equipamiento): self
    {
        $this->equipamiento = $equipamiento;

        return $this;
    }

    public function getPeso(): ?string
    {
        return $this->peso;
    }

    public function setPeso(?string $peso): self
    {
        $this->peso = $peso;

        return $this;
    }

    public function getPesoMax(): ?string
    {
        return $this->pesoMax;
    }

    public function setPesoMax(?string $pesoMax): self
    {
        $this->pesoMax = $pesoMax;

        return $this;
    }

    public function getCubiertas(): ?string
    {
        return $this->cubiertas;
    }

    public function setCubiertas(?string $cubiertas): self
    {
        $this->cubiertas = $cubiertas;

        return $this;
    }

    public function getMotor(): ?string
    {
        return $this->motor;
    }

    public function setMotor(?string $motor): self
    {
        $this->motor = $motor;

        return $this;
    }
}
