<?php

namespace App\BackendBundle\Repository;

use App\BackendBundle\Entity\ea__categorias;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ea__categorias|null find($id, $lockMode = null, $lockVersion = null)
 * @method ea__categorias|null findOneBy(array $criteria, array $orderBy = null)
 * @method ea__categorias[]    findAll()
 * @method ea__categorias[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ea__categoriasRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ea__categorias::class);
    }

    public function getAllByOrderAsc(){
        return $this->findBy(array(),array('orden' => 'ASC'));
    }

}
