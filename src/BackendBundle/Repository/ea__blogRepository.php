<?php

namespace App\BackendBundle\Repository;

use App\BackendBundle\Entity\ea__blog;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ea__blog|null find($id, $lockMode = null, $lockVersion = null)
 * @method ea__blog|null findOneBy(array $criteria, array $orderBy = null)
 * @method ea__blog[]    findAll()
 * @method ea__blog[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ea__blogRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ea__blog::class);
    }

    // /**
    //  * @return ea__blog[] Returns an array of ea__blog objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ea__blog
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
