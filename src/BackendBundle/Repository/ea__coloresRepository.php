<?php

namespace App\BackendBundle\Repository;

use App\BackendBundle\Entity\ea__colores;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ea__colores|null find($id, $lockMode = null, $lockVersion = null)
 * @method ea__colores|null findOneBy(array $criteria, array $orderBy = null)
 * @method ea__colores[]    findAll()
 * @method ea__colores[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ea__coloresRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ea__colores::class);
    }

    // /**
    //  * @return ea__colores[] Returns an array of ea__colores objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ea__colores
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
