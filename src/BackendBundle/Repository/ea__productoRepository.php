<?php

namespace App\BackendBundle\Repository;

use App\BackendBundle\Entity\ea__producto;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ea__producto|null find($id, $lockMode = null, $lockVersion = null)
 * @method ea__producto|null findOneBy(array $criteria, array $orderBy = null)
 * @method ea__producto[]    findAll()
 * @method ea__producto[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ea__productoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ea__producto::class);
    }

    public function getProductsbyCategoryId($categoria){
        return $this->createQueryBuilder('m')
            ->innerJoin('m.categoria', 'v')
            ->where('v.id = :hts')
            ->setParameter('hts', $categoria)
            ->getQuery()->getResult();
    }

    public function getProductsbyCategoryName($categoria){
        return $this->createQueryBuilder('m')
            ->innerJoin('m.categoria', 'v')
            ->where('v.nombre = :hts')
            ->setParameter('hts', $categoria)
            ->getQuery()->getResult();
    }

 
}
