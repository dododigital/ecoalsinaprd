<?php

namespace App\BackendBundle\Repository;

use App\BackendBundle\Entity\ea__beneficio;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ea__beneficio|null find($id, $lockMode = null, $lockVersion = null)
 * @method ea__beneficio|null findOneBy(array $criteria, array $orderBy = null)
 * @method ea__beneficio[]    findAll()
 * @method ea__beneficio[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ea__beneficioRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ea__beneficio::class);
    }

    // /**
    //  * @return ea__beneficio[] Returns an array of ea__beneficio objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ea__beneficio
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
