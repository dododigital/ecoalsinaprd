<?php

namespace App\BackendBundle\Repository;

use App\BackendBundle\Entity\ea__tags;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ea__tags|null find($id, $lockMode = null, $lockVersion = null)
 * @method ea__tags|null findOneBy(array $criteria, array $orderBy = null)
 * @method ea__tags[]    findAll()
 * @method ea__tags[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ea__tagsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ea__tags::class);
    }

    // /**
    //  * @return ea__tags[] Returns an array of ea__tags objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ea__tags
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
