<?php

namespace App\BackendBundle\Repository;

use App\BackendBundle\Entity\ea__banners;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ea__banners|null find($id, $lockMode = null, $lockVersion = null)
 * @method ea__banners|null findOneBy(array $criteria, array $orderBy = null)
 * @method ea__banners[]    findAll()
 * @method ea__banners[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ea__bannersRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ea__banners::class);
    }

    // /**
    //  * @return ea__banners[] Returns an array of ea__banners objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ea__banners
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
